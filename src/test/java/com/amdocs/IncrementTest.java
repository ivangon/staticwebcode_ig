package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;


public class IncrementTest {
    @Test
    public void testGetCounter() throws Exception {

        int k= new Increment().getCounter();
        assertEquals("get", 1, k);
        
    }
   
    @Test
    public void testDecreaseCounter() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("decrease 0", 1, k);

	int s= new Increment().decreasecounter(1);
        assertEquals("decrease 0", 1, s);

	int t= new Increment().decreasecounter(2);
        assertEquals("decrease 0", 1, t);


    }
}
